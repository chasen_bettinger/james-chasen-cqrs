# #	Default Config File #
_ = require 'lodash'
base = require './base'

PROJECT_NAME =		process.env.PROJECT_NAME		? "DEVIQ"
PROJECT_ABBREV =	process.env.PROJECT_ABBREV		? "IQ"

# ENVIRONMENT VARIABLES
HOST =				process.env.HOST               ? "localhost"
PORT =				process.env.PORT               ? 9101
STATIC_DIR =		process.env.STATIC_DIR         ? "./html_root"
IS_LOCALHOST =		HOST is 'localhost'
PROTOCOL =			"http#{if IS_LOCALHOST then '' else 's'}://"
API_URL = 			"#{PROTOCOL}#{HOST}#{if IS_LOCALHOST then ":#{PORT}" else ""}"

# MONGO
MONGO_URL =			process.env.MONGO_URL          ? "mongodb://localhost:27017"
MONGO_DB_NAME =		process.env.MONGO_DB_NAME      ? "lamb-#{PROJECT_ABBREV}-local"
MONGO_CONNECTION_STRING = "#{MONGO_URL}/#{MONGO_DB_NAME}?w=1&journal=false"

# MYSQL 

MYSQL_HOST =		process.env.MYSQL_HOST			? "localhost"
MYSQL_PORT =		process.env.MYSQL_PORT			? "3306"
MYSQL_USER =		process.env.MYSQL_USER			? "root"
MYSQL_PASS =		process.env.MYSQL_PASS			? "password"
MYSQL_DBNAME =		process.env.MYSQL_DBNAME		? "#{PROJECT_ABBREV}_dev_#{PORT}"
LEVEL2 =			process.env.LEVEL2 is "true"

# TIMING
LONG_POLL_TIMEOUT =	process.env.LONG_POLL_TIMEOUT  ? 30000
LONG_POLL_TIMEOUT =	Number (LONG_POLL_TIMEOUT ? 60000 * .5) # 30s
PUSH_INTERVAL =		process.env.PUSH_SVC_POLL_MS   ? 500

config =
	log_opts: 
		no_route_logs: true

	api:
		port: PORT
		static_file_server:
			directory: STATIC_DIR
		longPollTimeout: LONG_POLL_TIMEOUT

	perf:
		test_user_override: true

	lamd:
		connect_url: MONGO_CONNECTION_STRING

	db:
		mysql:
			pool:
				host:           MYSQL_HOST
				port:           MYSQL_PORT
				user:           MYSQL_USER
				password:       MYSQL_PASS
				database:       MYSQL_DBNAME
				level2_debug:   LEVEL2
		mongo:
			pool:
				health:
					connect_url:	MONGO_CONNECTION_STRING
	push_service:
		poll_interval: PUSH_INTERVAL


module.exports = _.merge base, config