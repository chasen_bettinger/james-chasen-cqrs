blueprint= require 'blueprint'
options = require './serverOptions'

{ 
	services, 
	routes, 
	mysql 
} = options

blueprint.start true, services, routes, true, mysql, true
.then (kit)-> # JCS: Add HTTPS support
	process.on 'SIGINT', ->
		d = new Date
		console.log d.toUTCString() + ' SIGINT'
		kit.services.RunQueue?.Drain()
	console.log 'Blueprint Node.js in service'
	#console.log kit.services.config
	#process.exit(1)

process.on 'unhandledRejection', (reason, p) ->
	d = new Date
	console.error(d.toUTCString() + ' unhandledRejection:', {reason, p})

process.on 'uncaughtException', (err) ->
	d = new Date
	console.error(d.toUTCString() + ' uncaughtException:', err.message)
	console.error(err.stack)
	process.exit(198)

process.on 'warning', (e) ->
	d= new Date
	console.error "#{d.toUTCString()} warning:", name:e.name, message: e.message, stack: e.stack

process.on 'beforeExit', (code) ->
	d= new Date
	console.error("#{d.toUTCString()} onBeforeExit Code:", code)

process.on 'exit', (code) ->
	d = new Date
	console.error("#{d.toUTCString()} onExit Code:", code)
