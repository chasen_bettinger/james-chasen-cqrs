require  'mocha'
fs = require 'fs'
Promise = require 'bluebird'
chai = require 'chai'

{
    services
    route_mods
    mysql_mods
} = require './serverOptions'

blueprint= require 'blueprint'
resource = 'BEFORE_MOCHA_TEST:';

global.should = chai.should();

# // CRB: 10/09/19: Read the sql dump and reset the database each time you run tests
# const readSqlDump = () => {
#   const f = '#{resource}readSqlDump::';
#   const options = {
#     encoding: 'utf-8',
#   };
#   return new Promise((resolve) => {
#     fs.readFile('./db/bootstrap.sql', options, (err, data)=> {
#       console.log(f, {err, data});
#       if (err) {
#         throw new Error('ERROR IN DUMP SQL');
#       }
#       resolve(data);
#     });
#   });
# };

before => 
  core = false;
  dbContents = false;
  ctx = {
    log: {
      debug: console.log,
    },
  };
  global.ctx = ctx;
  f = "#{resource}";
  console.log(f, 'starting test server!!');
  blueprint.start(true, services, [], true, mysql, false)
  .then (kit)->
    global.kit = kit;
#   core = kit.services.db.mysql.core;
#   ctx.conn = await core.acquire();
#   dbContents = await readSqlDump();
#   const {sqlQuery} = core;
#   await sqlQuery(ctx, dbContents);
